<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matkul_mahasiswa".
 *
 * @property integer $id
 * @property integer $id_matkul
 * @property integer $id_mahasiswa
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Mahasiswa $idMahasiswa
 * @property Matkul $idMatkul
 */
class MatkulMahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matkul_mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_matkul', 'id_mahasiswa', 'created_at', 'updated_at'], 'required'],
            [['id_matkul', 'id_mahasiswa', 'created_at', 'updated_at'], 'integer'],
            [['id_mahasiswa'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['id_mahasiswa' => 'id']],
            [['id_matkul'], 'exist', 'skipOnError' => true, 'targetClass' => Matkul::className(), 'targetAttribute' => ['id_matkul' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_matkul' => 'Id Matkul',
            'id_mahasiswa' => 'Id Mahasiswa',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMahasiswa()
    {
        return $this->hasOne(Mahasiswa::className(), ['id' => 'id_mahasiswa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMatkul()
    {
        return $this->hasOne(Matkul::className(), ['id' => 'id_matkul']);
    }
}
