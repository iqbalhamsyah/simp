<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "perwalian".
 *
 * @property integer $id
 * @property integer $tahun
 * @property integer $semester
 * @property string $keterangan
 * @property integer $waktu_dibuat
 * @property integer $waktu_disunting
 *
 * @property Matkul[] $matkuls
 */
class Perwalian extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_dibuat',
                'updatedAtAttribute' => 'waktu_disunting',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perwalian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'semester'], 'required'],
            [['tahun', 'semester', 'waktu_dibuat', 'waktu_disunting'], 'integer'],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tahun' => 'Tahun',
            'semester' => 'Semester',
            'keterangan' => 'Keterangan',
            'waktu_dibuat' => 'Waktu Dibuat',
            'waktu_disunting' => 'Waktu Disunting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatkuls()
    {
        return $this->hasMany(Matkul::className(), ['id_perwalian' => 'id']);
    }

    public function getNama()
    {
        return 'Perwalian Tahun ' . $this->tahun . ' Semester ' . $this->semester;
    }
}
