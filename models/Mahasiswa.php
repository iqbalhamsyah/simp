<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property integer $id
 * @property integer $id_prodi
 * @property string $tahun_ajaran
 * @property string $nama
 * @property integer $jenis_kelamin
 * @property integer $tanggal_lahir
 * @property integer $waktu_dibuat
 * @property integer $waktu_disunting
 *
 * @property Prodi $idProdi
 * @property MatkulMahasiswa[] $matkulMahasiswas
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    const PRIA = 10;
    const WANITA = 20;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_dibuat',
                'updatedAtAttribute' => 'waktu_disunting',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_prodi', 'tahun_ajaran', 'jenis_kelamin', 'tanggal_lahir'], 'required'],
            [['id_prodi', 'jenis_kelamin', 'waktu_dibuat', 'waktu_disunting'], 'integer'],
            ['tanggal_lahir', 'safe'],
            // regex untuk format Tahun Ajaran, e.g 2016/2017
            ['tahun_ajaran', 'match', 'pattern' => '/\d{4}[\/]\d{4}/'],
            [['nama'], 'string', 'max' => 255],
            [['id_prodi'], 'exist', 'skipOnError' => true, 'targetClass' => Prodi::className(), 'targetAttribute' => ['id_prodi' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_prodi' => 'Prodi',
            'tahun_ajaran' => 'Tahun Ajaran',
            'nama' => 'Nama',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tanggal_lahir' => 'Tanggal Lahir',
            'waktu_dibuat' => 'Waktu Dibuat',
            'waktu_disunting' => 'Waktu Disunting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdi()
    {
        return $this->hasOne(Prodi::className(), ['id' => 'id_prodi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatkulMahasiswas()
    {
        return $this->hasMany(MatkulMahasiswa::className(), ['id_mahasiswa' => 'id']);
    }

    public static function getJenisKelaminList()
    {
        return [
            self::PRIA => 'Laki-laki',
            self::WANITA => 'Perempuan',
        ];
    }

    public function getNamaJenisKelamin()
    {
        if ($this->jenis_kelamin === self::PRIA) {
            return 'Laki-laki';
        } elseif ($this->jenis_kelamin === self::WANITA) {
            return 'Perempuan';
        }
    }
}
