<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "matkul_induk".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $waktu_dibuat
 * @property integer $waktu_disunting
 *
 * @property Matkul[] $matkuls
 */
class MatkulInduk extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_dibuat',
                'updatedAtAttribute' => 'waktu_disunting',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matkul_induk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['waktu_dibuat', 'waktu_disunting'], 'integer'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'waktu_dibuat' => 'Waktu Dibuat',
            'waktu_disunting' => 'Waktu Disunting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatkuls()
    {
        return $this->hasMany(Matkul::className(), ['id_matkul_induk' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'nama');
    }
}
