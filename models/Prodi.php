<?php

namespace app\models;

use Yii;
use yii\behaviors\TimeStampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "prodi".
 *
 * @property integer $id
 * @property integer $id_jenjang
 * @property integer $waktu_dibuat
 * @property integer $waktu_disunting
 *
 * @property Mahasiswa[] $mahasiswas
 * @property Jenjang $idJenjang
 */
class Prodi extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_dibuat',
                'updatedAtAttribute' => 'waktu_disunting',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prodi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jenjang', 'nama'], 'required'],
            ['nama', 'string', 'max' => 255],
            [['id_jenjang', 'waktu_dibuat', 'waktu_disunting'], 'integer'],
            [['id_jenjang'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::className(), 'targetAttribute' => ['id_jenjang' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_jenjang' => 'Jenjang',
            'nama' => 'Nama Prodi',
            'waktu_dibuat' => 'Waktu Dibuat',
            'waktu_disunting' => 'Waktu Disunting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswas()
    {
        return $this->hasMany(Mahasiswa::className(), ['id_prodi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::className(), ['id' => 'id_jenjang']);
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'nama');
    }
}
