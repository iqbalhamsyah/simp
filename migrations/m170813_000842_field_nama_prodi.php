<?php

use yii\db\Migration;

class m170813_000842_field_nama_prodi extends Migration
{
    public function up()
    {
        $this->addColumn('{{%prodi}}', 'nama', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%prodi}}', 'nama');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
