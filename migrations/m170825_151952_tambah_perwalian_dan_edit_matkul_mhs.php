<?php

use yii\db\Migration;

class m170825_151952_tambah_perwalian_dan_edit_matkul_mhs extends Migration
{
    public function up()
    {
        // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%perwalian}}', [
            'id' => $this->primaryKey(),
            'tahun' => $this->integer(5)->notNull(),
            'semester' => $this->smallInteger()->notNull(),
            'keterangan' => $this->string(255)->null(),
            'waktu_dibuat' => $this->integer()->notNull(),
            'waktu_disunting' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addColumn('{{%matkul}}', 'id_perwalian', $this->integer()->notNull());

        $this->dropColumn('{{%matkul}}', 'semester');
        $this->dropColumn('{{%matkul}}', 'tahun');

        $this->createIndex('idx_id_perwalian_matkul', '{{%matkul}}', 'id_perwalian');

        $this->addForeignKey('fk-matkul-id_perwalian-perwalian-id', '{{%matkul}}', 'id_perwalian', '{{%perwalian}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk-matkul-id_perwalian-perwalian-id', '{{%matkul}}');

        $this->dropColumn('{{%matkul}}', 'id_perwalian');

        $this->addColumn('{{%matkul}}', 'semester', $this->smallInteger()->notNull());
        $this->addColumn('{{%matkul}}', 'tahun', $this->integer(5)->notNull());

        $this->dropTable('{{%perwalian}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
