<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Perwalian */

$this->title = Yii::t('app', 'Tambah Perwalian');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perwalian'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perwalian-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
