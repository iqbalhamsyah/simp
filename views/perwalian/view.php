<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Perwalian */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perwalian'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary perwalian-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Perwalian <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tahun',
            'semester',
            'keterangan',
            'waktu_dibuat:datetime',
            'waktu_disunting:datetime',
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Sunting'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        </p>
    </div>

</div>
