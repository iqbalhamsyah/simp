<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Yourtable */

$this->title = 'Sunting Yourtable';
$this->params['breadcrumbs'][] = ['label' => 'Yourtable', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="yourtable-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
