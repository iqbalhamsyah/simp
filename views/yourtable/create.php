<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Yourtable */

$this->title = 'Tambah Yourtable';
$this->params['breadcrumbs'][] = ['label' => 'Yourtable', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yourtable-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
