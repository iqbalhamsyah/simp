<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\MatkulInduk;
use app\models\Dosen;

/* @var $this yii\web\View */
/* @var $model app\models\Matkul */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary matkul-form">
    <div class="box-header with-border">
        <h3 class="box-title">Form matkul</h3>
    </div>
    <div class="box-body">
    <?php $form = ActiveForm::begin([
                        'layout'=>'horizontal',
                        'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'wrapper' => 'col-sm-4',
                            'error' => '',
                            'hint' => '',
                    ],
                    ]]); ?>

    <?= $form->field($model, 'id_matkul_induk')->dropDownList(MatkulInduk::getList()); ?>

    <?= $form->field($model, 'id_dosen')->dropDownList(Dosen::getList()); ?>

    <?= $form->field($model, 'sks')->textInput() ?>

    </div>
    <div class="box-footer with-border form-group">
        <div class="col-sm-3 col-sm-offset-3">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-flat']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
