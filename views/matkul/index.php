<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MatkulSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Matkul';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary matkul-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('Tambah Matkul', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            [
                'attribute'=>'id_matkul_induk',
                'value'=>function($data) {
                    return $data->matkulInduk->nama;
                },
            ],
            [
                'attribute'=>'id_dosen',
                'value'=>function($data) {
                    return $data->dosen->nama;
                },
            ],
            'semester',
            'tahun',
            // 'sks',
            // 'waktu_dibuat',
            // 'waktu_disunting',

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
