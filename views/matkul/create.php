<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Matkul */

$this->title = 'Tambah Matkul';
$this->params['breadcrumbs'][] = ['label' => 'Matkul', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matkul-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
