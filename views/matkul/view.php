<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Matkul */

$this->title = $model->matkulInduk->nama . ' Semester ' . $model->semester . ' Tahun ' . $model->tahun;
$this->params['breadcrumbs'][] = ['label' => 'Matkul', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary matkul-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Matkul <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id_matkul_induk',
                'value'=>function($data) {
                    return $data->matkulInduk->nama;
                },
            ],
            [
                'attribute'=>'id_dosen',
                'value'=>function($data) {
                    return $data->dosen->nama;
                },
            ],
            'semester',
            'tahun',
            'sks',
            'waktu_dibuat:datetime',
            'waktu_disunting:datetime',
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Yakin Akan Menghapus Data?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

</div>
