<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MatkulInduk */

$this->title = 'Tambah Matkul Induk';
$this->params['breadcrumbs'][] = ['label' => 'Matkul Induk', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matkul-induk-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
