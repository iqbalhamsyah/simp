<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary mahasiswa-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('Tambah Mahasiswa', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            [
                'attribute'=>'id_prodi',
                'value'=>function($data) {
                    return $data->prodi->nama;
                },
            ],
            'tahun_ajaran',
            'nama',
            [
                'attribute'=>'jenis_kelamin',
                'value'=>function($data) {
                    return $data->namaJenisKelamin;
                },
            ],
            /*[
                'attribute'=>'tanggal_lahir',
                'value'=>function($data) {
                    return Helper::getTanggalSingkat($data->tanggal_lahir);
                },
            ],*/
            // 'waktu_dibuat',
            // 'waktu_disunting',

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
