<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Mahasiswa */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Mahasiswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary mahasiswa-view">
    <div class="box-header with-border">
        <h1 class="box-title">Mahasiswa : <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id_prodi',
                'value'=>function($data) {
                    return $data->prodi->nama;
                },
            ],
            'tahun_ajaran',
            'nama',
            [
                'attribute' => 'jenis_kelamin',
                'value' => function($data) {
                    return $data->namaJenisKelamin;
                }
            ],
            [
                'attribute'=>'tanggal_lahir',
                'value'=>function($data) {
                    return Helper::getTanggalSingkat($data->tanggal_lahir);
                },
            ],
            'waktu_dibuat:datetime',
            'waktu_disunting:datetime',
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Yakin Akan Menghapus Data?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

</div>
