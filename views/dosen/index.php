<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Dosen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary dosen-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('Tambah Dosen', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            'nip',
            'nama',
            [
                'attribute'=>'tanggal_lahir',
                'value'=>function($data) {
                    return Helper::getTanggalSingkat($data->tanggal_lahir);
                },
            ],
            [
                'attribute' => 'jenis_kelamin',
                'value' => function($data) {
                    return $data->namaJenisKelamin;
                }
            ],
            // 'foto',
            // 'waktu_dibuat',
            // 'waktu_disunting',

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
